<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <form action="client.php" method="post">
        <div style="text-align: center; margin-top: 10%">
            <button type="submit" class="btn btn-primary">Generar CSV</button>
        </div>
    </form>

    <br><br>

    <!-- Mensaje -->
    <?php
    if (isset($_GET['estado']) && $_GET['estado'] == 1) {
        echo "<div class='alert alert-success' role='alert'>Archivo CSV Creado Satisfactoriamente !
                       </div>";

        echo "Descargar: <a href=datos.csv>datos.csv</a>";
    }

    ?>
</body>

</html>