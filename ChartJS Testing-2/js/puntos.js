const GRAFICOPUNTOS = document.getElementById("miGraficoPuntos");


const PARPUNTOS = [{
    x: 11.9,
    y: 185
}, {
    x: 14.2,
    y: 215
}, {
    x: 16.4,
    y: 325
}, {
    x: 15.2,
    y: 332
}, {
    x: 18.5,
    y: 406
}, {
    x: 22.1,
    y: 552
}, {
    x: 19.4,
    y: 412
}, {
    x: 25.1,
    y: 614
}, {
    x: 23.4,
    y: 544
}, {
    x: 22.6,
    y: 445
}, {
    x: 17.2,
    y: 408
}, {
    x: 18.1,
    y: 421
},]




document.getElementById("tabla1").innerHTML = [11.9, 14.2, 16.4, 15.2, 18.5, 22.1, 19.4, 25.1, 23.4, 22.6, 17.2, 18.1].join("<br>");
document.getElementById("tabla2").innerHTML = [185, 215, 325, 332, 406, 552, 412, 614, 544, 445, 408, 421].join("<br>");



let graficaPuntos = new Chart(GRAFICOPUNTOS, {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'Ganancia vs Temperatura',
            data: PARPUNTOS,
            backgroundColor: 'rgb(0,174,239)'
        }],
    },
    options: {
        plugins: {
            legend: {
              position: 'left',
            },
            title: {
                display: true,
                text: 'Ganancia de venta de helados en funcion de temeperatura',
                position: 'top'
            },
       responsive: false
        
    },
        scales: {
            x: {
                type: 'linear',
                position: 'bottom',
                display: true,
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function (value, index, values) {
                        return value + '\xB0C';
                    }
                }
            },
            y: {
                display: true,
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function (value, index, values) {
                        return '$' + value;
                    }
                }
            },
            
        },
    }
});

document.getElementById('download-pdf').addEventListener("click", downloadPDF);
var canvas = document.querySelector('#miGraficoPuntos');

function downloadPDF() {
    var canvas = document.querySelector('#miGraficoPuntos');
      //creates image
      var canvasImg = canvas.toDataURL("image/png", 1.0);
      //creates PDF from img
      var doc = new jsPDF();
      doc.setFontSize('9')
      doc.text(9, 9, "PDF generado con jsPDF");
      doc.text(9, 290, "PDF generado con jsPDF");
      doc.line(0, 12, 1000, 12);
      doc.line(0, 285, 1000, 285);
      doc.page=1;
      doc.text(190,290, 'Pagina ' + doc.page);
      doc.addImage(canvasImg, 'PNG', 10, 45, 190, 190);
      doc.save('canvas.pdf');
  }