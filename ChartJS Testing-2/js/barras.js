const GRAFICOBARRA = document.getElementById("miGraficoBarra");
const MONTANAS = ['K2', 'Makalu', 'Manaslu', 'Nanga Parbat', 'Lhotse', 'Cho Oyu', 'Dhaulagiri I', 'Everest', 'Kangchenjunga', 'Annapurna I']; 
const ALTURAS = [8611, 8485, 8163, 8126, 8516, 8188, 8167, 8849, 8586, 8091];

document.getElementById("tabla1").innerHTML = MONTANAS.join('<br>');
document.getElementById("tabla2").innerHTML = ALTURAS.join('<br>');

let graficaBarra = new Chart(GRAFICOBARRA, {
    type: 'bar',
    data: {
        labels: MONTANAS,
        datasets: [{
          label: "Altura (m)",
          data: ALTURAS,
          backgroundColor: [
            'rgba(0,174,239, 0.2)'
          ],
          borderColor: [
            'rgb(0,174,239)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        plugins: {
            legend: {
              position: 'left',
            },
            title: {
              display: true,
              text: 'Las 10 monta\u00f1as mas altas del mundo',
              position: 'top'
          },

        },
        scales: {
          y: {
              beginAtZero: false
          }
      }
    } 
});

document.getElementById('download-pdf').addEventListener("click", convertir_html2pdf);

function convertir_html2pdf() {
  const $elementoParaConvertir = document.body; // <-- Aquí puedes elegir cualquier elemento del DOM
  html2pdf()
      .set({
          margin: 1,
          filename: 'grafica_de_barras.pdf',
          image: {
              type: 'jpeg',
              quality: 2
          },
          html2canvas: {
              scale: 4, // A mayor escala, mejores gráficos, pero más peso
              letterRendering: true,
          },
          jsPDF: {
              unit: "in",
              format: "a4",
              orientation: 'portrait' // landscape o portrait
          }
      })
      .from($elementoParaConvertir)
      .save()
      .catch(err => console.log(err));
};

var canvas = document.querySelector('#miGraficoBarra');
function downloadPDF() {
    var canvas = document.querySelector('#miGraficoBarra');
      //creates image
      var canvasImg = canvas.toDataURL("image/png", 1.0);
      //creates PDF from img
      var doc = new jsPDF();
      doc.setFontSize('9')
      doc.text(9, 9, "PDF generado con jsPDF");
      doc.text(9, 290, "PDF generado con jsPDF");
      doc.line(0, 12, 1000, 12);
      doc.line(0, 285, 1000, 285);
      doc.page=1;
      doc.text(190,290, 'Pagina ' + doc.page);
      doc.addImage(canvasImg, 'PNG', 10, 45, 190, 190);
      doc.save('canvas.pdf');
  }