const GRAFICORADAR = document.getElementById("miGraficoRadar");

const ATRIBUTOS = [
  'Confianza',
  'Dribbling',
  'Precision',
  'Posicionamiento',
  'Velocidad',
  'Potencia',
];

const INICIAL = [65, 65, 70, 81, 75, 65];
const ACTUAL = [85, 80, 80, 75, 95, 75];
const ESPERADO = [90, 90, 90, 90, 90, 90];

document.getElementById("tabla1").innerHTML = ATRIBUTOS.join('<br>');
document.getElementById("tabla2").innerHTML = INICIAL.join('<br>');
document.getElementById("tabla3").innerHTML = ACTUAL.join('<br>');
document.getElementById("tabla4").innerHTML = ESPERADO.join('<br>');

let graficaRadar = new Chart(GRAFICORADAR, {
    type: 'radar',
    data: {
        labels: ATRIBUTOS,
        datasets: [{
          label: 'Rendimiento Inicial',
          data: INICIAL,
          fill: true,
          backgroundColor: 'rgb(255,0,0, 0.2)',
          borderColor: 'rgb(255,0,0)',
          pointBackgroundColor: 'rgb(255,0,0)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgb(255,0,0)'
        }, {
          label: 'Rendimiento Actual',
          data: ACTUAL,
          fill: true,
          backgroundColor: 'rgba(54, 162, 235, 0.2)',
          borderColor: 'rgb(54, 162, 235)',
          pointBackgroundColor: 'rgb(54, 162, 235)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgb(54, 162, 235)'
        },
        {
            label: 'Rendimiento Esperado',
            data: ESPERADO,
            fill: true,
            backgroundColor: 'rgba(50,205,50, 0.2)',
            borderColor: 'rgb(50,205,50)',
            pointBackgroundColor: 'rgb(50,205,50)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgb(50,205,50)'
          }]
      },
    options: {
      elements: {
        line: {
          borderWidth: 3
        }
      },
      plugins: {
        title: {
            display: true,
            text: 'Rendimiento Futbolistico',
            position: 'top'
        }
    }
    }   
});

document.getElementById('download-pdf').addEventListener("click", downloadPDF);
var canvas = document.querySelector('#miGraficoRadar');

function downloadPDF() {
    var canvas = document.querySelector('#miGraficoRadar');
      //creates image
      var canvasImg = canvas.toDataURL("image/png", 1.0);
      //creates PDF from img
      var doc = new jsPDF();
      doc.setFontSize('9')
      doc.text(9, 9, "PDF generado con jsPDF");
      doc.text(9, 290, "PDF generado con jsPDF");
      doc.line(0, 12, 1000, 12);
      doc.line(0, 285, 1000, 285);
      doc.page=1;
      doc.text(190,290, 'Pagina ' + doc.page);
      doc.addImage(canvasImg, 'PNG', 10, 45, 190, 190);
      doc.save('canvas.pdf');
  }