var config = { columnWidth: 20, columnHeight: 235, columnGap: 10, padding: 100 };

var datos = [{ nombre: "Aventura", valor: 63.57 }, { nombre: "Accion", valor: 47.72 }, { nombre: "Drama", valor: 36.89 }, { nombre: "Comedia", valor: 34.34 }, { nombre: "Suspenso", valor: 20.33 }, { nombre: "Horror", valor: 11.84 }, { nombre: "Romanticos", valor: 9.99 }, { nombre: "Musicales", valor: 4.11 }, { nombre: "Documental", valor: 2.24 }, { nombre: "Humor Negro", valor: 1.57 }]

renderData(datos);

function renderData(datos) {
    var NUM_COLUMNAS = datos.length;
    config.width = NUM_COLUMNAS * (config.columnWidth + config.columnGap) + (2 * config.padding);
    config.height = config.columnHeight + 2 * config.padding;
    var VALOR_MAX = d3.max(datos, function (d) { return +d.valor; });

    var x = d3.scale.ordinal()
        .rangeRoundBands([0, config.width - 2 * config.padding])
        .domain(datos.map(function (d) { return d.nombre; }));

    var y = d3.scale.linear()
        .range([0, config.columnHeight])
        .domain([0, VALOR_MAX]);

    var rangeY = d3.scale.linear()
        .range([config.columnHeight, 0])
        .domain([0, VALOR_MAX]);

    var ejeX = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var ejeY = d3.svg.axis()
        .scale(rangeY)
        .orient("left");

    var svg = d3.select("svg")
        .attr("width", config.width)
        .attr("height", config.height);
    //Título
    svg.append("text")
        .attr("x", config.width / 2)
        .attr("y", config.height - config.padding - 2 - config.columnHeight - (config.padding / 2))
        .style("text-anchor", "middle")
        .text("Ingresos de diferentes generos del cine en USA");
    //Etiqueta
    svg.append("text")
        .attr("x", 20)
        .attr("y", config.height - config.padding - 2 - config.columnHeight - 10)
        .style("text-anchor", "start")
        .style("font-size", ".6em")
        .text("Billones de U$S");

    svg.append("g")
        .attr("class", "eje")
        .attr("transform", "translate(" + config.padding + "," + (10 + config.padding + config.columnHeight) + ")")
        .call(ejeX)
        .selectAll("text")
        .attr("transform", "rotate(90)")
        .attr("x", "10")
        .attr("y", "-3")
        .style("text-anchor", "start");


    svg.append("g")
        .attr("class", "eje")
        .attr("transform", "translate(" + (config.padding - 10) + "," + config.padding + ")")
        .call(ejeY);

    svg.selectAll("rect")
        .data(datos)
        .enter().append("rect")
        .attr("width", config.columnWidth)
        .attr("x", function (d, i) { return config.padding + x(d.nombre) })
        .attr("y", function (d, i) { return config.padding + config.columnHeight - y(d.valor) })
        .attr("height", function (d, i) { return y(d.valor) })
        .attr("data-nombre", function (d, i) { return d.nombre })
        .attr("data-valor", function (d, i) { return +d.valor })
    // Generación de la tabla dinamicamente.
    let info = document.querySelector('#info');
    info.innerHTML = '';

    for (let item of datos) {
        info.innerHTML += `
        <tr>
            <td>${item.nombre}</td>
            <td>${item.valor}</td>
        </tr>
        `
    }
}

