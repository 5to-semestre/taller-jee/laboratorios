const GRAFICORADAR = document.getElementById("miGraficoRadar");

let graficaRadar = new Chart(GRAFICORADAR, {
    type: 'radar',
    data: {
        labels: [
          'Confianza',
          'Dribbling',
          'Precision',
          'Posicionamiento',
          'Velocidad',
          'Potencia',
        ],
        datasets: [{
          label: 'Rendimiento Inicial',
          data: [65, 65, 70, 81, 75, 65],
          fill: true,
          backgroundColor: 'rgb(255,0,0, 0.2)',
          borderColor: 'rgb(255,0,0)',
          pointBackgroundColor: 'rgb(255,0,0)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgb(255,0,0)'
        }, {
          label: 'Rendimiento Actual',
          data: [85, 80, 80, 75, 95, 75],
          fill: true,
          backgroundColor: 'rgba(54, 162, 235, 0.2)',
          borderColor: 'rgb(54, 162, 235)',
          pointBackgroundColor: 'rgb(54, 162, 235)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgb(54, 162, 235)'
        },
        {
            label: 'Rendimiento Esperado',
            data: [90, 90, 90, 90, 90, 90],
            fill: true,
            backgroundColor: 'rgba(50,205,50, 0.2)',
            borderColor: 'rgb(50,205,50)',
            pointBackgroundColor: 'rgb(50,205,50)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgb(50,205,50)'
          }]
      },
    options: {
      elements: {
        line: {
          borderWidth: 3
        }
      },
      plugins: {
        title: {
            display: true,
            text: 'Rendimiento Futbolistico',
            position: 'top'
        }
    }
    }   
});

document.getElementById('download-pdf').addEventListener("click", downloadPDF);
var canvas = document.querySelector('#miGraficoRadar');

function downloadPDF() {
    var canvas = document.querySelector('#miGraficoRadar');
      //creates image
      var canvasImg = canvas.toDataURL("image/png", 1.0);
      //creates PDF from img
      var doc = new jsPDF();

      doc.addImage(canvasImg, 'PNG', 10, 10, 190, 190);
      doc.save('canvas.pdf');
  }